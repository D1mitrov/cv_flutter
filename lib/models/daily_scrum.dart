import 'package:flutter/material.dart';

class DailyScrum {
  final String id;
  final String title;
  final String experience;
  final String description;
  final ThemeData theme;
  final DateTime datedebut;
  final DateTime datefin;

  DailyScrum({
    required this.id,
    required this.title,
    required this.experience,
    required this.description,
    required this.theme,
    required this.datedebut,
    required this.datefin,
  });
}

import 'package:flutter/material.dart';
import 'daily_scrum.dart';

class Data {
  final String title;
  final List<DailyScrum> items;

  Data({required this.title, required this.items});

  static List<Data> sampleData() {
    return [
      Data(
        title: "Professionnel",
        items: [
          DailyScrum(
            id: '1',
            title: "Stage",
            experience: "mind & go",
            description: "lorem ipsum",
            theme: ThemeData(primarySwatch: Colors.yellow),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
          DailyScrum(
            id: '2',
            title: "Alternance",
            experience: "Cartier Patrimoine",
            description: "lorem ipsum",
            theme: ThemeData(primarySwatch: Colors.yellow),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
        ],
      ),
      Data(
        title: "Formation",
        items: [
          DailyScrum(
            id: '3',
            title: "Bac",
            experience: "Bac général",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
          DailyScrum(
            id: '4',
            title: "Formation",
            experience: "Formation développeur web",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
          DailyScrum(
            id: '5',
            title: "Alternance",
            experience: "Alternance développeur d'application",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
        ],
      ),
      Data(
        title: "Hobbies",
        items: [
          DailyScrum(
            id: '6',
            title: "Voiture",
            experience: "Voiture",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
          DailyScrum(
            id: '7',
            title: "Musculation",
            experience: "Musculation",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
          DailyScrum(
            id: '8',
            title: "Jeux vidéos",
            experience: "Jeux vidéos",
            description: "Description complètement random",
            theme: ThemeData(primarySwatch: Colors.orange),
            datedebut: DateTime.now(),
            datefin: DateTime.now(),
          ),
        ],
      ),
    ];
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/daily_scrum.dart';

class DetailView extends StatelessWidget {
  final DailyScrum scrum;

  DetailView({required this.scrum});

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat.yMd();

    return Scaffold(
      appBar: AppBar(
        title: Text(scrum.title),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("Date: ", style: TextStyle(color: Colors.blue)),
                Text(dateFormatter.format(scrum.datedebut)),
                Text(" - "),
                Text(dateFormatter.format(scrum.datefin)),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              children: [
                Text("Titre: ", style: TextStyle(color: Colors.blue)),
                Text(scrum.title),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              children: [
                Text("Description: ", style: TextStyle(color: Colors.blue)),
                Text(scrum.description),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              children: [
                Text("Entreprise: ", style: TextStyle(color: Colors.blue)),
                Text(scrum.experience),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

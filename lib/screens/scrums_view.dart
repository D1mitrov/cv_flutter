import 'package:flutter/material.dart';
import '../models/daily_scrum.dart';
import '../widgets/card_view.dart';

class ScrumsView extends StatelessWidget {
  final List<DailyScrum> scrums;

  ScrumsView({required this.scrums});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mon cv"),
      ),
      body: ListView.builder(
        itemCount: scrums.length,
        itemBuilder: (context, index) {
          final scrum = scrums[index];
          return CardView(scrum: scrum);
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/daily_scrum.dart';

class CardView extends StatelessWidget {
  final DailyScrum scrum;

  CardView({required this.scrum});

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat.yMd();

    return Card(
      margin: EdgeInsets.all(8.0),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  scrum.title,
                ),
                Spacer(),
                Column(
                  children: [
                    Text(dateFormatter.format(scrum.datedebut)),
                    Text(dateFormatter.format(scrum.datefin)),
                  ],
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Text(
              scrum.description,
            ),
          ],
        ),
      ),
    );
  }
}
